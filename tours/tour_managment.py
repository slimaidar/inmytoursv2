from tourcms import Connection
from tours import models
from tours.models import Gallery


def add_tour_from_tourcms(market_place_id, private_key, tour_id, channel_id):
    conn = Connection(market_place_id, private_key, "dict")
    api_tour = conn.show_tour(tour_id,channel_id)['tour']
    if models.Tour.objects\
            .filter(api_info__tour_id=api_tour['tour_id'])\
            .filter(api_info__account_id=api_tour['account_id'])\
            .filter(api_info__channel_id=api_tour['channel_id'])\
            .exists():
        return 1
    try:
        country = models.Country.objects.get(code=api_tour['country'])
    except:
        country = models.Country.objects.create(code=api_tour['country'])

    tour_price = models.TourPrice.objects.create()
    for rate in api_tour['new_booking']['people_selection']['rate']:
        if rate['rate_id'] == 'r1': #Adults
            tour_price.label_adult = rate['label_2']
            tour_price.price_adult = round(int(float(rate['from_price'])))
        elif rate['rate_id'] == 'r2': #Children
            tour_price.label_children = rate['label_2']
            tour_price.price_children = round(int(float(rate['from_price'])))
        elif rate['rate_id'] == 'r3': #Infants
            tour_price.label_infant = rate['label_2']
            tour_price.price_infant = round(int(float(rate['from_price'])))
        else:   #Seniors --> r4
            tour_price.label_senior = rate['label_2']
            tour_price.price_senior = round(int(float(rate['from_price'])))
    tour_price.save()


    times = {}
    if 'start_time' in api_tour:
        times['start_time'] = api_tour['start_time']
    if 'end_time' in api_tour:
        times['end_time'] = api_tour['end_time']

    available_days = []
    for day in models.ALLOWED_DAYS:
        for d in api_tour['available'].split(', '):
            if str(d) == str(day[1]):
                available_days.append(day[0])


    api_info = models.ApiTours.objects.create(
        tour_id = api_tour['tour_id'],
        account_id = api_tour['account_id'],
        channel_id = api_tour['channel_id'],
        tour_code = api_tour['tour_code']
    )

    api_info.image = api_tour['images']['image'][0]['url_thumbnail']
    if "geo_code_start" in api_tour:
        api_info.geo_code_start = api_tour['geo_code_start']
    if "geo_code_end" in api_tour:
        api_info.geo_code_end = api_tour['geo_code_end']
    api_info.save()

    tour_desc = models.TourDesc.objects.create(
        language = models.Language.objects.get(title='en'),
        title = api_tour['tour_name_long'],
        description = api_tour['longdesc'],
        summary = api_tour['summary'],
        include = api_tour['inc'].split('\n'),
        exclude = api_tour['ex'].split('\n'),
        highlights = api_tour['exp'].split('\n'),
        notes = api_tour['essential'].split('\n'),
        essential_info = api_tour['pick'].split('\n'),
    )
    tour_desc.save()

    tour = models.Tour.objects.create()
    if 'tour_name_long' in api_tour:
        tour.title = api_tour['tour_name_long']
    tour.featured = False
    tour.active = False
    tour.category = api_tour['product_type']
    tour.country = country
    tour.duration = models.TourDuration.objects.create(
        days=api_tour['duration']
    )
    tour.prices = tour_price
    tour.times = times
    tour.available_days = available_days
    tour.is_from_api = True
    tour.api_info = api_info
    tour.tour_desc = tour_desc
    tour.booking_size = {
        "min_booking_size": api_tour['min_booking_size'],
        "max_booking_size": api_tour['max_booking_size'],
    }

    tour.save()

    # gallery
    for img in api_tour['images']['image']:
        gallery = Gallery.objects.create()
        gallery.api_image = img['url_large']
        gallery.tour = tour
        gallery.save()

    return 2

