from django.db import models
from django.contrib.postgres.fields import ArrayField, JSONField
from multiselectfield import MultiSelectField
from .utils import unique_slug_generator
from django.db.models.signals import pre_save

ALLOWED_DAYS = (
    ('0','Sunday'),
    ('1','Monday'),
    ('2','Tuesday'),
    ('3','Wednesday'),
    ('4','Thursday'),
    ('5','Friday'),
    ('6','Saturday'),
    ('7', 'Daily'),
)
STARS = (
        ('1',1),
        ('2', 2),
        ('3', 3),
        ('4', 4),
        ('5', 5),
)
CATEGORIES = (
    ('0', 'Experience'),
    ('1', 'Accommodation hotel,campsite,villa,ski,chalet,lodge'),
    ('2', 'Transport,Transfer'),
    ('3', 'Tour/cruise including overnight stay'),
    ('4', 'Day tour/trip/activity/attraction (No overnight stay'),
    ('5', 'Tailor made'),
    ('6', 'Event'),
    ('7', 'Training/education'),
    ('8', 'Other'),
    ('9', 'Restaurant / Meal alternative'),
)

DAYS_IN_ADVANCE = (
    ('0','None'),
)
for i in range (1,31):
    DAYS_IN_ADVANCE+= (("{}".format(i),"{}".format(i)),)


class Tour(models.Model):
    title = models.CharField(max_length=200, help_text="This title is only for admins")
    feautured = models.BooleanField(default=False, help_text="check this one if you want it to be displayed in the home page")
    active = models.BooleanField(default=True, help_text="show on the website?")
    image = models.ImageField(upload_to='image/tours', null=True, blank=True)
    stars = models.CharField(max_length=200, choices=STARS, null=True, blank=True)
    category = models.CharField(max_length=220, choices=CATEGORIES)
    country = models.ForeignKey('tours.Country', on_delete=models.SET_NULL, blank=True, null=True)
    duration = models.ForeignKey('tours.TourDuration', on_delete=models.SET_NULL, blank=True, null=True)
    prices = models.ForeignKey('tours.TourPrice', on_delete=models.SET_NULL, blank=True, null=True)
    times = JSONField(null=True, blank=True)  #  {start_time|end_time}
    available_days = MultiSelectField(choices=ALLOWED_DAYS, null=True, blank=True)
    is_from_api = models.BooleanField(default=False, null=True, blank=True)
    days_in_advance = models.CharField(max_length=20, choices=DAYS_IN_ADVANCE, null=True, blank=True)
    api_info = models.ForeignKey('tours.ApiTours', on_delete=models.SET_NULL, blank=True, null=True)
    # tour_desc = models.OneToOneField('tours.TourDesc', on_delete=models.SET_NULL, blank=True, null=True)
    booking_size = JSONField(null=True, blank=True) #{min_booking_size | max_booking_size}

    def __str__(self):
        return str(self.title)


class TourDesc(models.Model):
    language = models.ForeignKey(
        'tours.Language', on_delete=models.SET_NULL, blank=True, null=True)
    title = models.CharField(max_length=200)
    description = models.TextField()
    summary = models.TextField()
    include = ArrayField(
        models.CharField(max_length=200), null=True, blank=True)  # inc
    exclude = ArrayField(
        models.CharField(max_length=200), null=True, blank=True)  # ex
    highlights = ArrayField(
        models.CharField(max_length=200), null=True, blank=True)  # exp
    notes = ArrayField(
        models.CharField(max_length=200, null=True, blank=True))  # essential
    essential_info = ArrayField(
        models.CharField(max_length=200, null=True, blank=True))  # pick
    selection = models.ManyToManyField('tours.TourSelection', blank=True)
    slug = models.SlugField(unique=True, blank=True, null=True,
                            help_text="leave it empty if you don't have a good keyword in your head :)")
    tour = models.ForeignKey('tours.Tour', on_delete=models.SET_NULL, blank=True, null=True)

    def __str__(self):
        return self.title

class Gallery(models.Model):
    image = models.ImageField(upload_to='static/img/tours', null=True, blank=True)
    api_image = models.CharField(max_length=200, null=True)
    tour = models.ForeignKey(Tour, on_delete=models.CASCADE, null=True, blank=True)


class Country(models.Model):
    name = models.CharField(max_length=200, null=True)
    code = models.CharField(max_length=2)
    flag = models.ImageField(upload_to='static/img/countries', null=True, blank=True)

    def __str__(self):
        return str(self.name)


class City(models.Model):
    name        = models.CharField(max_length=200)
    description = models.TextField(blank=True, null=True)
    image       = models.ImageField(blank=True, null=True)

    def __str__(self):
        return self.name


class ApiTours(models.Model):
    tour_id = models.PositiveIntegerField()
    account_id = models.PositiveIntegerField()
    channel_id = models.PositiveIntegerField()
    tour_code = models.CharField(max_length=100, null=True, blank=True)
    image = models.CharField(max_length=400, null=True, blank=True)
    geo_code_start = models.CharField(max_length=100, null=True, blank=True)
    geo_code_end = models.CharField(max_length=100, null=True, blank=True)

    unique_together = ('tour_id', 'account_id', 'channel_id')

    def __str__(self):
        return str(self.tour_id)

class TourPrice(models.Model):
    label_senior = models.CharField(max_length=100, blank=True, null=True, help_text="Eg: 90+")
    price_senior = models.IntegerField(blank=True, null=True, default=0)
    label_adult = models.CharField(max_length=100, blank=True, null=True, help_text="Eg: 14-50")
    price_adult = models.IntegerField(blank=True, null=True, default=0)
    label_children = models.CharField(max_length=100, blank=True, null=True, help_text="Eg: 14-50")
    price_children = models.IntegerField(blank=True, null=True, default=0)
    label_infant = models.CharField(max_length=100, blank=True, null=True, help_text="Eg: 14-50")
    price_infant = models.IntegerField(blank=True, null=True, default=0)


class TourDuration(models.Model):
    hours = models.CharField(max_length=100, blank=True, null=True,
                             help_text="Leave it empty if it is not availble on the tour")
    minutes = models.CharField(max_length=100, blank=True, null=True,
                               help_text="Leave it empty if it is not availble on the tour")
    days = models.CharField(max_length=100, blank=True, null=True,
                            help_text="Leave it empty if it is not availble on the tour")
    nights = models.CharField(max_length=100, blank=True, null=True,
                             help_text="Leave it empty if it is not availble on the tour")


class Language(models.Model):
    title = models.CharField(max_length=3, unique=True)
    image = models.ImageField(upload_to='languages')

    def __str__(self):
        return self.title

class TourSelection(models.Model):
    tour = models.ForeignKey('tours.Tour', on_delete=models.CASCADE)
    selection = models.CharField(max_length=200)
    price = models.PositiveIntegerField()

    def __str__(self):
        return self.selection

def tourdesc_pre_save(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)

pre_save.connect(tourdesc_pre_save, sender=TourDesc)
