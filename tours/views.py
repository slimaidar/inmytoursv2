from django.shortcuts import render
from django.views.generic import DetailView
from .models import TourDesc, Gallery


class TourDetailView(DetailView):
    model = TourDesc
    template_name = "tours/tour_detail.html"
    context_object_name = "tour"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        tour_detail = self.get_object()
        context["gallery"] = Gallery.objects.filter(tour=tour_detail.tour)
        return context