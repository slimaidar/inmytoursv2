from django.contrib import admin
from . import models

admin.site.register(models.Tour)
admin.site.register(models.TourDesc)
admin.site.register(models.ApiTours)
admin.site.register(models.City)
admin.site.register(models.TourPrice)
admin.site.register(models.TourDuration)
admin.site.register(models.Language)
admin.site.register(models.Country)
admin.site.register(models.Gallery)
admin.site.register(models.TourSelection)
