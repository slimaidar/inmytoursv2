from django.db import models
from django.contrib.auth.models import AbstractUser


class CUser(AbstractUser):
    is_provider = models.BooleanField(default=False)
    is_agency = models.BooleanField(default=False)
    is_guide = models.BooleanField(default=False)