from django.shortcuts import render, get_object_or_404
from django.views.generic import TemplateView
from tourcms import Connection
from tours import models,tour_managment
from django.utils import translation

from tours.models import Language


class HomePageView(TemplateView):
    template_name = 'pages/home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        tour_status = tour_managment.add_tour_from_tourcms(45047,"4e1a35717330", 31, 9159)
        context['tour_status'] = tour_status
        lang = get_object_or_404(Language, title=translation.get_language())
        context['tours'] = models.TourDesc.objects.filter(tour__feautured=True)\
                                    .filter(tour__active=True)\
                                    .filter(language=lang)
        return context
